<?php require_once "./code.php" ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S04: Access Modifier and Encapsulation</title>
</head>
<body>
	<h1>Access Modifier</h1>
	<p><?php var_dump ($building) ?></p>

	<p><?php echo $building->getName(); ?></p> 

	<?php $building->setName('Casywn Building') ?>
	<p><?php echo $building->getName(); ?></p> 
	<p><?php echo $building->getFloors(); ?></p> 

 	<p><?php var_dump ($condominium); ?></p>
	<p><?php echo $condominium->getName(); ?></p>

	<p><?php var_dump ($milk); ?></p>
	<p><?php echo $milk->getName(); ?></p>
	<p><?php echo $milk->setName("Bear Brand"); ?></p>
	<p><?php echo $milk->getName(); ?></p>

	<p><?php var_dump ($kopiko); ?></p>
	<p><?php echo $kopiko->getName(); ?></p>



</body>
</html>